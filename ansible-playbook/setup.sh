PORT=22 && docker run -d --rm --name local-vps-$PORT -p $PORT:$PORT -p 80:80 atlekbai/local-vps $PORT
ssh-copy-id root@127.0.0.1

# first command in ansible
ansible -i hosts.ini lb -m ping

# command that shows free space in memory
ansible -i hosts.ini lb -a "free -h"

# running our fist playbook
ansible-playbook -i hosts.ini playbook.yml
