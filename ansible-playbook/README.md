# ansible-playbook

## Screenshots

### Running setup.sh

![p1](https://user-images.githubusercontent.com/54265853/180967903-b773994a-5693-4231-ab82-4ca38358ec04.png)

### Checking nginx installed

![nginx](https://user-images.githubusercontent.com/54265853/180973365-77b0df6f-6da6-471f-9306-80dcc90420ff.png)
